package clases.ito.poo;

import java.time.LocalDate;

public class CuentaBancaria {
	public CuentaBancaria(long numCuenta, String nomCliente, float saldo, LocalDate fechaApertura,
			LocalDate fechaActualizacion) {
		super();
		this.numCuenta = numCuenta;
		this.nomCliente = nomCliente;
		this.saldo = saldo;
		this.fechaApertura = fechaApertura;
		this.fechaActualizacion = fechaActualizacion;
	}


	private long numCuenta=0;
	private String nomCliente="";
	private float saldo=0f;
	private LocalDate fechaApertura=null;
	private LocalDate fechaActualizacion=null;
	
	
	public CuentaBancaria() {
		super();
		//System.out.println("nkcancb");
	}
	public CuentaBancaria(long numCuenta, String nomCliente, float saldo, LocalDate fechaApertura) {
		super();
		this.numCuenta = numCuenta;
		this.nomCliente = nomCliente;
		this.saldo = saldo;
		this.fechaApertura = fechaApertura;
	}
	
	
	public boolean deposito(float cantidad) {
		boolean deposito=false;
		return deposito;
	}
	public boolean retiro(float cantidad) {
		boolean retiro=false;
		return retiro;
	}
	
	
	
	public long getNumCuenta() {
		return numCuenta;
	}
	public void setNumCuenta(long numCuenta) {
		this.numCuenta = numCuenta;
	}
	public String getNomCliente() {
		return nomCliente;
	}
	public void setNomCliente(String nomCliente) {
		this.nomCliente = nomCliente;
	}
	public float getSaldo() {
		return saldo;
	}
	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
	public LocalDate getFechaApertura() {
		return fechaApertura;
	}
	public void setFechaApertura(LocalDate fechaApertura) {
		this.fechaApertura = fechaApertura;
	}
	
	
	@Override
	public String toString() {
		return "CuentaBancaria [numCuenta=" + numCuenta + ", nomCliente=" + nomCliente + ", saldo=" + saldo
				+ ", fechaApertura=" + fechaApertura + ", fechaActualizacion=" + fechaActualizacion + "]";
	}

}
